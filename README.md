Um das Tool nutzen zu können, benötigt man AutoHotkey: https://www.autohotkey.com/download/

Falls das Script mit einer neuren Version nicht funktionieren sollte, empfehle ich die Version zu verwenden, mit der ich dieses Tool entwickelt habe: https://www.autohotkey.com/download/1.1/AutoHotkey_1.1.30.03.zip

Falls die Links sich mal ändern und nicht mehr funktionieren sollten, einfach in Google nach AutoHotkey suchen.

Bugs melden
mailto:incoming+gl-user007-valheim-tool-28368236-issue-@incoming.gitlab.com
