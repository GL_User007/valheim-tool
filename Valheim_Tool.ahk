﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.


Global SteamPath := ""
RegRead, SteamPath, HKEY_CURRENT_USER\SOFTWARE\Valve\Steam, SteamPath
Global ValheimFolder := SubStr(A_AppData,1,StrLen(A_AppData)-7) . "LocalLow\IronGate\Valheim"
Global ValheimCharacterFolder := ValheimFolder . "\characters\"
Global ValheimWorldFolder := ValheimFolder . "\worlds"



; Hier könnt Ihr Eure Valheim-Server oder Seeds hinterlegen (Nach der Eingabe von z.B. #vhs1 wird sofort alles ersetzt)
:*:#vhs1::IP-Adresse:Port
:*:#vhs2::IP-Adresse:Port
:*:#s1::Seed1
:*:#s2::Seed2



; Erstellt ein Backup eines Charakters
; Hotkey: Win + B
#b::
InputBox, ValheimCharacter, Valheim Character Backup, Wie lautet der Name Deines Charakters?
If (ErrorLevel)
	{
	Exit
	}
ValheimCharacterFile := ValheimCharacterFolder . ValheimCharacter . ".fch"
ValheimCharacterBackupFile := ValheimCharacterFolder . ValheimCharacter . ".fch.backup"
If (FileExist(ValheimCharacterFile))
	{
	FileCopy,% ValheimCharacterFile,% ValheimCharacterBackupFile, 1
	MsgBox,% 64+4096, Valheim Character Backup, Dein Character %ValheimCharacter% wurde erfolgreich gesichert.
	}
Else
	{
	MsgBox,% 48+4096, Valheim Character Backup, Es existiert leider kein Charakter mit dem Namen %ValheimCharacter%.
	}
Return



; Stellt einen zuvor gesicherten Charakter wieder her
; Hotkey: Win + R
#r::
InputBox, ValheimCharacter, Valheim Character Backup, Wie lautet der Name Deines Charakters?
If (ErrorLevel)
	{
	Exit
	}
ValheimCharacterFile := ValheimCharacterFolder . ValheimCharacter . ".fch"
ValheimCharacterBackupFile := ValheimCharacterFolder . ValheimCharacter . ".fch.backup"
If (FileExist(ValheimCharacterBackupFile))
	{
	FileCopy,% ValheimCharacterBackupFile,% ValheimCharacterFile, 1
	MsgBox,% 64+4096, Valheim Character Backup, Dein Character %ValheimCharacter% wurde erfolgreich wiederhergestellt.
	}
Else
	{
	MsgBox,% 48+4096, Valheim Character Backup, Zum Charakter %ValheimCharacter% existiert leider keine Sicherung, die wiederhergestellt werden könnte.
	}
Return



; Öffnet die Konsole im Spiel und stoppt das aktuelle Event
; Hotkey: Win + S
#s::
Send, {F5}
Sleep, 250
Send, killall
Sleep, 250
Send, {Enter}
Sleep, 250
Send, stopevent
Sleep, 250
Send, {Enter}
Sleep, 250
Send, {F5}
Return



; Öffnet die Konsole im Spiel und blendet das Menü vom Mod "Build Share" ein und aus
; Hotkey: Win + U
#u::
Send, {F5}
Sleep, 250
Send, bs ui
Sleep, 250
Send, {Enter}
Sleep, 250
Send, {F5}
Return



; Öffnet die Konsole im Spiel und schaltet den Cheatmodus ein und aus
; Hotkey: Win + D
#d::
Send, {F5}
Sleep, 250
Send, devcommands
Sleep, 250
Send, {Enter}
Sleep, 250
Send, {F5}
Return



; Öffnet die Konsole im Spiel und erhöht alle Skills auf 100
; Hotkey: Win + A
#a::
Skills := ["axes","blocking","bows","woodcutting","knives","run","clubs","sneak","swords","swim","spears","pickaxes","jump","unarmed","polearms","FireMagic","FrostMagic","NatureMagic","HolyMagic"]
Send, {F5}
Sleep, 250
Loop
	{
	Skill := "raiseskill " . Skills[A_Index] . " 100"
	Send, %Skill%
	Sleep, 250
	Send, {Enter}
	Sleep, 250
	} Until A_Index == Skills.Count()
Send, {F5}
Return



; Schaltet Valheim Plus durch Änderung des BepInEx Ordners ein und aus
; Hotkey: Win + P
#p::
If (FileExist(SteamPath . "\steamapps\common\Valheim\BepInEx_"))
	{
	FileMoveDir, %SteamPath%\steamapps\common\Valheim\BepInEx_, %SteamPath%\steamapps\common\Valheim\BepInEx, R
	If (FileExist(SteamPath . "\steamapps\common\Valheim\BepInEx"))
		{
		MsgBox,% 64+4096, Valheim Plus Activator, Valheim Plus wurde aktiviert.
		}
	}
Else If (FileExist(SteamPath . "\steamapps\common\Valheim\BepInEx"))
	{
	FileMoveDir, %SteamPath%\steamapps\common\Valheim\BepInEx, %SteamPath%\steamapps\common\Valheim\BepInEx_, R
	If (FileExist(SteamPath . "\steamapps\common\Valheim\BepInEx_"))
		{
		MsgBox,% 64+4096, Valheim Plus Activator, Valheim Plus wurde deaktiviert.
		}
	}
Return



; Öffnet die Valheim Plus Konfigurationsdatei
; Hotkey: Win + C
#c::
Run, notepad.exe %SteamPath%\steamapps\common\Valheim\BepInEx\config\valheim_plus.cfg
Return



; Öffnet den Valheim Ordner
; Hotkey: Win + O
#o::
Run, %SteamPath%\steamapps\common\Valheim
Return



; Öffnet den Weltordner
; Hotkey: Win + W
#w::
Run, %ValheimWorldFolder%
Return
